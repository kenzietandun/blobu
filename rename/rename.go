package rename

import (
	_"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"regexp"
	"strings"
)

func FindFiles(path string) []string {

	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	var filenames []string

	for _, f := range files {
		filenames = append(filenames, f.Name())
	}

	return filenames

}

func IsAnime(path string) bool {

	videoExtensions := [...]string{".mkv", ".mp4"}

	for _, extension := range videoExtensions {
		if strings.HasSuffix(path, extension) {
			return true
		}
	}

	return false

}

func RemoveAnimeTags(filename string) string {

	// Removes opening tags usually indicating fansub group name
	openingBracket := `^[\[\(\{]`
	matched, _ := regexp.MatchString(openingBracket, filename)
	if matched {
		openingBracketRegex := regexp.MustCompile(`^\[\w+(-\w+)?\]\s+`)
		filename = openingBracketRegex.ReplaceAllString(filename, "")
	}

	// Removes tag in form of [AOFJ241F]
	tagNum := `(\s+)?\[[A-Z\d]{8}\]`
	matched, _ = regexp.MatchString(tagNum, filename)
	if matched {
		tagNumRegex := regexp.MustCompile(tagNum)
		filename = tagNumRegex.ReplaceAllString(filename, "")
	}

	// Removes quality tag
	qualityTag := `(\s+)?\[\d+p\]`
	matched, _ = regexp.MatchString(qualityTag, filename)
	if matched {
		qualityTagRegex := regexp.MustCompile(qualityTag)
		filename = qualityTagRegex.ReplaceAllString(filename, "")
	}

	return filename

}

func CreateAnimeDirectory(directory string, filename string) string {
	// Remove episode number from anime title
	episodeNum := `\s?-\s?\d+(.*)?.mkv$`
    matched, _ := regexp.MatchString(episodeNum, filename)
	if matched {
		episodeNumRegex := regexp.MustCompile(episodeNum)
		filename = episodeNumRegex.ReplaceAllString(filename, "")
	}
    
    newDirname := path.Join(directory, filename)
    os.MkdirAll(newDirname, 0644)
    return newDirname
}

func RenameFiles(dir []string) {
	for _, directory := range dir {
		filenames := FindFiles(directory)
		for _, file := range filenames {
			if IsAnime(file) {
				newFilename := RemoveAnimeTags(file)
				oldPath := path.Join(directory, file)
                newDir := CreateAnimeDirectory(directory, newFilename)
                // Checks if newDir is a directory name, not a filename
                if !IsAnime(newDir) {
                    newPath := path.Join(newDir, newFilename)
                    os.Rename(oldPath, newPath)
                }
			}
		}
	}

}
