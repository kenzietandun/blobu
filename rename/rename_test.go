package rename

import (
	"testing"
)

func TestIsAnime(t *testing.T) {
	var FileIsAnime bool

	testCaseOne := "[Commie] Chihayafuru 3 - 11 [7505303F].mkv"

	FileIsAnime = IsAnime(testCaseOne)
	if !FileIsAnime {
		t.Errorf(testCaseOne + " should be detected as anime, but not")
	}

	testCaseTwo := "My Super Cool Cooking Anime.mp4"

	FileIsAnime = IsAnime(testCaseTwo)
	if !FileIsAnime {
		t.Errorf(testCaseTwo + " should be detected as anime, but not")
	}

	testCaseThree := "Movie.avi"

	FileIsAnime = IsAnime(testCaseThree)
	if FileIsAnime {
		t.Errorf(testCaseThree + " should not be detected as anime, but is")
	}
}

func TestRenameAnime(t *testing.T) {

	testCaseOne := "[Commie] Chihayafuru 3 - 11 [7505303F].mkv"
	var renamedName string

	renamedName = RenameAnime(testCaseOne)
	correctRenamedName := "Chihayafuru 3 - 11.mkv"
	if renamedName != correctRenamedName {
		t.Errorf(testCaseOne + " is renamed as " + renamedName + ", but should be " + correctRenamedName + " instead")
	}

	testCaseTwo := "[PAS] Beastars - 12 (WEB 1080p AAC) [C5922928].mkv"
	correctRenamedName = "Beastars - 12.mkv"
	renamedName = RenameAnime(testCaseTwo)

	if renamedName != correctRenamedName {
		t.Errorf(testCaseTwo + " is renamed as " + renamedName + ", but should be " + correctRenamedName + " instead")
	}

	testCaseThree := "[HorribleSubs] Sword Art Online - Alicization - War of Underworld - 12 [1080p].mkv"
	correctRenamedName = "Sword Art Online - Alicization - War of Underworld - 12.mkv"
	renamedName = RenameAnime(testCaseThree)

	if renamedName != correctRenamedName {
		t.Errorf(testCaseThree + " is renamed as " + renamedName + ", but should be " + correctRenamedName + " instead")
	}

}
