package main

import (
	"gitlab.com/kenzietandun/blobu/rename"
	"os"
)

func main() {
	args := os.Args[1:]
	if len(args) > 0 {
        rename.RenameFiles(args)
	}
}
